*************************************************************************************

    France | 2019-2-7 17:24:06

    I hereby agree to the terms of the Mobicoop Contributor License
    Agreement.

    I declare that I am authorized and able to make this agreement and sign
    this declaration.

    Signed,

    Remi Wortemann
    remi.wortemann@gmail.com | Remi Wortemann

*************************************************************************************